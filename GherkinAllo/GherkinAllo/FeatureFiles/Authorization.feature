﻿Feature: Authorization
	As a user
	I want to access to login to my account
	In order to view the status of my orders

	Background: 
	Given https://allo.ua website is open

Scenario: Login to your personal Allo account
	When User clicks on button authorization
	When User enter in fild e-mail "dionirana@gmail.com"
	When User enter in fild password "qwert12345"
	When User clicks on button Enter
	Then User is logged in

	Scenario: Switching from authorization form to registration form
	When User clicks on button authorization
	When User clicks on button registration
	Then Registration form is open