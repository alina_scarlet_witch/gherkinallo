﻿Feature: Cart

	As a user
	I want to have access to my cart
	In order to view a content of my order any time 

Scenario: Unauthorized user has empty cart
	Given https/allo website is open
	When User clicks on cart
	Then Cart is empty
