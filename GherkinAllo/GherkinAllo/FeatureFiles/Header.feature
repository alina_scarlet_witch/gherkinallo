﻿Feature: Header
	As a user
	I want to have access change the city of the website
	In order to view a list of goods in stock 

	Background: 
	Given https://allo.ua website is open

	Scenario: Сhange the location of the city to the Dnepr
	When User clicks on location of the city
	When User clicks on the city Dnepr
	When User clicks on location of the city
	Then The location of the city has changed to the Dnepr

	Scenario: Go to the promotion page
	When User clicks on button promotion
	Then The promotion page is open

	Scenario: Go to the cut in price page
	When User clicks on button cut in price
	Then The cut in price page is open

	Scenario: Go to the contact page 
	When User clicks on button contact
	Then The contact page is open

	Scenario: Go to the warranty / return page
	When User clicks on button warranty/return
	Then The warranty/return page is open

	Scenario: Go to the credit page
	When User clicks on button Credit
	Then The credit page is open

	Scenario: Go to the shipment and payment page
	When User clicks on button shipment and payment
	Then The shipment and payment page is open

	Scenario: Show contacts from the home page
	When User clicks on number phone 
	Then Phone numbers are displayed

	Scenario: Searching for items on demand phones
	When User enters the text Phone in the search field
	Then Items on the Phone request are displayed

	
	