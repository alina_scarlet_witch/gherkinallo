﻿using GherkinAllo.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace GherkinAllo.Steps
{
    [Binding]
    public class HeaderSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public PromotionPage promotionPage;
        public CutInPricePage cutInPricePage;
        public Contacts contacts;
        public WarrantyAndRefundPage warrantyAndRefundPage;
        public CreditPage creditPage;
        public DeliveryAndPayPage deliveryAndPayPage;
        public SearchPage searchPage;

        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(5);
        }

        [Given(@"https://allo.ua website is open")]
        public void GivenHttpsAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            promotionPage = new PromotionPage(driver);
            cutInPricePage = new CutInPricePage(driver);
            contacts = new Contacts(driver);
            warrantyAndRefundPage = new WarrantyAndRefundPage(driver);
            creditPage = new CreditPage(driver);
            deliveryAndPayPage = new DeliveryAndPayPage(driver);
            searchPage = new SearchPage(driver);
        }

        [When(@"User clicks on location of the city")]
        public void WhenUserClicksOnLocationOfTheCity()
        {
            mainPage.ClickLocationButtonOnMainPage();
        }
        
        [When(@"User clicks on the city Dnepr")]
        public void WhenUserClicksOnTheCityDnepr()
        {
            mainPage.ClickLocationDneprButtonOnMainPage();
        }

        [Then(@"The location of the city has changed to the Dnepr")]
        public void ThenTheLocationOfTheCityHasChangedToTheDnepr()
        {
            string mainLocationLabel = mainPage.GetTextMainLocationLabel();
            Assert.AreEqual("Дніпро", mainLocationLabel);
        }

        [When(@"User clicks on button promotion")]
        public void WhenUserClicksOnButtonPromotion()
        {
            mainPage.ClickPromotionsButtonOnMainPage();
        }

        [Then(@"The promotion page is open")]
        public void ThenThePromotionPageIsOpen()
        {
            string mainLabelPromotion = promotionPage.GetTextMainLabelPromotion();
            Assert.AreEqual("Акції", mainLabelPromotion);
        }

        [When(@"User clicks on button cut in price")]
        public void WhenUserClicksOnButtonCutInPrice()
        {
            mainPage.ClickCutInPriceButtonOnMainPage();
        }

        [Then(@"The cut in price page is open")]
        public void ThenTheCutInPricePageIsOpen()
        {
            string mainLabelCutInPrice = cutInPricePage.GetTextCutInPriceMainLabel();
            Assert.AreEqual("РОЗПРОДАЖ ТОВАРІВ З УЦІНКОЮ", mainLabelCutInPrice);
        }

        [When(@"User clicks on button contact")]
        public void WhenUserClicksOnButtonContact()
        {
            mainPage.ClickContactsButtonOnMainPage();
        }

        [Then(@"The contact page is open")]
        public void ThenTheContactPageIsOpen()
        {
            string contactsMainLabel = contacts.GetTextContactsMainLabel(); ;
            Assert.AreEqual("Консультації та замовлення за телефонами", contactsMainLabel);
        }

        [When(@"User clicks on button warranty/return")]
        public void WhenUserClicksOnButtonWarrantyReturn()
        {
            mainPage.ClickWarrantyAndRefundButtonOnMainPage();
        }

        [Then(@"The warranty/return page is open")]
        public void ThenTheWarrantyReturnPageIsOpen()
        {
            string warrantyReturnMainLabel = warrantyAndRefundPage.GetTextWarrantyAndRefundMainLabel(); ;
            Assert.AreEqual("Гарантійне обслуговування", warrantyReturnMainLabel);
        }

        [When(@"User clicks on button Credit")]
        public void WhenUserClicksOnButtonCredit()
        {
            mainPage.ClickCreditButtonOnMainPage();
        }

        [Then(@"The credit page is open")]
        public void ThenTheCreditPageIsOpen()
        {
            string creditMainLabel = creditPage.GetTextCreditMainLabel(); ;
            Assert.AreEqual("Кредит", creditMainLabel);
        }

        [When(@"User clicks on button shipment and payment")]
        public void WhenUserClicksOnButtonShipmentAndPayment()
        {
            mainPage.ClickDeliveryAndPayButtonOnMainPage();
        }

        [Then(@"The shipment and payment page is open")]
        public void ThenTheShipmentAndPaymentPageIsOpen()
        {
            string deliveryAndPayMainLabel = deliveryAndPayPage.GetTextDeliveryAndPayMainLabel(); ;
            Assert.AreEqual("Доставка і оплата", deliveryAndPayMainLabel);
        }

        [When(@"User clicks on number phone")]
        public void WhenUserClicksOnNumberPhone()
        {
            mainPage.ClickContactNumberButtonOnMainPage();
        }

        [Then(@"Phone numbers are displayed")]
        public void ThenPhoneNumbersAreDisplayed()
        {
            string contactNumberMainLabel = mainPage.GetTextContactNumberMainLabel(); ;
            Assert.AreEqual("(0-800) 300-100", contactNumberMainLabel);
        }

        [When(@"User enters the text Phone in the search field")]
        public void WhenUserEntersTheTextPhoneInTheSearchField()
        {
            mainPage.RequestInSearchInpCut("Телефон\n");
        }

        [Then(@"Items on the Phone request are displayed")]
        public void ThenItemsOnThePhoneRequestAreDisplayed()
        {
            string searchMainLabel = searchPage.GetTextSearchMainLabel(); ;
            Assert.AreEqual("Смартфони і мобільні телефони", searchMainLabel);
            
        }
    }
}
