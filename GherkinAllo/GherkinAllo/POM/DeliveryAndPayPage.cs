﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class DeliveryAndPayPage
    {
        private IWebDriver _driver;

        public DeliveryAndPayPage(IWebDriver driver)   
        {
            this._driver = driver;
        }

        public By DeliveryAndPayMainLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");

        public IWebElement FindDeliveryAndPayMainLabel()
        {
            return _driver.FindElement(DeliveryAndPayMainLabel);
        }
        public string GetTextDeliveryAndPayMainLabel()
        {
            return FindDeliveryAndPayMainLabel().Text;
        }
    }
}
