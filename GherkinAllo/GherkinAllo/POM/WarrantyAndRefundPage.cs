﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class WarrantyAndRefundPage
    {
        private IWebDriver _driver;

        public WarrantyAndRefundPage(IWebDriver driver)   
        {
            this._driver = driver;
        }

        public By WarrantyAndRefundMainLabel = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[1]/h2");

        public IWebElement FindWarrantyAndRefundMainLabel()
        {
            return _driver.FindElement(WarrantyAndRefundMainLabel);
        }
        public string GetTextWarrantyAndRefundMainLabel()
        {
            return FindWarrantyAndRefundMainLabel().Text;
        }
    }
}
