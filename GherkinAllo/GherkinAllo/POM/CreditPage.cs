﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class CreditPage
    {
        private IWebDriver _driver;

        public CreditPage(IWebDriver driver)  
        {
            this._driver = driver;
        }

        public By CreditMainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");

        public IWebElement FindCreditMainLabel()
        {
            return _driver.FindElement(CreditMainLabel);
        }
        public string GetTextCreditMainLabel()
        {
            return FindCreditMainLabel().Text;
        }
    }
}
