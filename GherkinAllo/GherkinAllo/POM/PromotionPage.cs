﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class PromotionPage
    {
        private IWebDriver _driver;

        public PromotionPage(IWebDriver driver) 
        { 
            this._driver = driver;
        }

        public By PromotionOnMainPage = By.XPath("/html/body/div[1]/div/div/div[2]/h1");

        public IWebElement FindMainLabelPromotion()
        {
            return _driver.FindElement(PromotionOnMainPage);
        }
        public string GetTextMainLabelPromotion()
        {
            return FindMainLabelPromotion().Text;
        }
    }
}
