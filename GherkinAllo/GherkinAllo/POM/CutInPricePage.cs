﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class CutInPricePage
    {
        private IWebDriver _driver;

        public CutInPricePage(IWebDriver driver)  
        {
            this._driver = driver;
        }

        public By CutInPriceMainLabel = By.XPath("/html/body/div[1]/header/div[3]/div/h1");

        public IWebElement FindCutInPriceMainLabel()
        {
            return _driver.FindElement(CutInPriceMainLabel);
        }
        public string GetTextCutInPriceMainLabel()
        {
            return FindCutInPriceMainLabel().Text;
        }
    }
}
