﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class SearchPage
    {
        private IWebDriver _driver;

        public SearchPage(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

        public By SearchMainLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[2]/div[1]/div[2]/a");

        public IWebElement FindSearchMainLabel()
        {
            return _driver.FindElement(SearchMainLabel);
        }
        public string GetTextSearchMainLabel()
        {
            return FindSearchMainLabel().Text;
        }
    }
}
