﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GherkinAllo.POM
{
    public class Contacts
    {
        private IWebDriver _driver;

        public Contacts(IWebDriver driver)   
        {
            this._driver = driver;
        }

        public By ContactsMainLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");

        public IWebElement FindContactsMainLabel()
        {
            return _driver.FindElement(ContactsMainLabel);
        }
        public string GetTextContactsMainLabel()
        {
            return FindContactsMainLabel().Text;
        }
    }
}
